application.controller('presentation', ['$scope', function($scope){
  $scope.slide_num = 0;
  $scope.controls = {};
  $scope.controls.prev = false;
  $scope.controls.next = true;

  $scope.prev = function(){

  }

  $scope.next = function(){

  }

  console.log(angular.element(document.querySelector('.page')));

  $scope.fullscreen = function(){
    if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement) {
      if (document.getElementById('presentation').requestFullscreen) {
        document.getElementById('presentation').requestFullscreen();
      } else if (document.getElementById('presentation').mozRequestFullScreen) {
        document.getElementById('presentation').mozRequestFullScreen();
      } else if (document.documentElement.webkitRequestFullscreen) {
        document.getElementById('presentation').webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
      }
    } else {
      if (document.cancelFullScreen) {
        document.cancelFullScreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
      }
    }
  }
}]);

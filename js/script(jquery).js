Dropzone.autoDiscover = false;
var filenum = 0;

$(document).pjax('a.nav-link', '#site-content');
$(document).pjax('a', '#pjax-container')

$("#song-upload-dropzone").dropzone({
    url: $(this).attr('action'),
    // maxFilesize: 5,
    acceptedFiles: 'audio/mp3',
    uploadMultiple: false,
    thumbnailWidth: '200',
    thumbnailHeight: '200',
    renameFilename: 'cleanFilename',
    dictDefaultMessage: 'Just drop your files here!',
    dictResponseError: 'Error uploading file!',
    addRemoveLinks: true,
    headers: {
        'X-CSRF-Token': $('input[name="_token"]').attr('value')
    },
    success: function(file,response){
      console.log(response);
    },
    init: function(){
      function showTags(url, id) {
        tags = ID3.getAllTags(url);
        console.log(id);
        var image = tags.picture;
        if (image) {
              var base64String = "";
              for (var i = 0; i < image.data.length; i++) {
                  base64String += String.fromCharCode(image.data[i]);
              }
              var base64 = "data:" + image.format + ";base64," +
              window.btoa(base64String);
              document.getElementById(id).setAttribute('src', base64);
            }
      }
      this.on("addedfile", function (file) {
        filenum++;
        // file.previewElement = Dropzone.createElement(this.options.previewTemplate);
        $(file.previewElement).find('.dz-details .dz-filename').html(file.name);
        $(file.previewElement).find('.dz-image img').attr({ "id": 'file_'+filenum });
        $(file.previewElement).find('.dz-error-mark').attr('data-dz-remove', '');
        $(file.previewElement).find('.dz-details .dz-size').html('');
        // // $(file.previewElement).find('.dz-image img').attr('src', );
        // $('#song-upload-dropzone').append(file.previewElement);
        if( file.type == 'audio/mp3' )
        ID3.loadTags(file.name, function() {
          showTags(file.name, 'file_'+filenum);
        }, {
          tags: ["title","artist","album","picture"],
          dataReader: ID3.FileAPIReader(file)
        });
      });
    }
});

var parent, ink, d, x, y;
$("ul li a").mousedown(function(e){
	parent = $(this).parent();
	//create .ink element if it doesn't exist
	if(parent.find(".ink").length == 0)
		parent.prepend("<span class='ink'></span>");

	ink = parent.find(".ink");
	//incase of quick double clicks stop the previous animation
	ink.removeClass("animate");

	//set size of .ink
	if(!ink.height() && !ink.width())
	{
		//use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
		d = Math.max(parent.outerWidth(), parent.outerHeight());
		ink.css({height: d, width: d});
	}

	//get click coordinates
	//logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
	x = e.pageX - parent.offset().left - ink.width()/2;
	y = e.pageY - parent.offset().top - ink.height()/2;

  ink.css({'top':x+'px','left':y+'px'}).addClass('animate');
});
	//set the position and add class .a

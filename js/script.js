var application = angular.module('application', [])
var ready = false;

angular.element(document).ready(function(){
  ready = true;
  console.log(ready);
});

application.controller('page', ['$scope', function($scope){
  var ready = $scope.ready = false;

  // $scope.$on('$viewContentLoaded', function() {
  //   this.ready = true;
  //   console.log(this);
  // });
}]);



application.controller('ajax-form', ['$scope', '$http', '$httpParamSerializerJQLike', function($scope, $http, $httpParamSerializerJQLike){
  $scope.form = {};
  $scope.response = {};
  $scope.save = function(action){
    $http({
      method: "POST",
      url: action,
      data: $httpParamSerializerJQLike($scope.form),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
    })
    .success(function(data){
      console.log(data);
      $scope.response = angular.fromJson(data);
      if($scope.response.status == 'success'){
        window.location.path('/');
      }
    });
    return false;
  }
  $scope.autofill_fix = function(){
    this.removeAttribute('readonly');
  }
}]);

$(document).ready(function(){
  var elem = document.getElementById('page');
  var scrollY = $('#page').scrollTop();
  $circle = '';
  $('body').on('mousedown', 'ul>li>a.nav-link', function(event){
    event.preventDefault();
    $element = $(this).parents('li');
    $circle = $("<div id='circle'></div>");

    $element.append($circle);

    var offset_top = $element.offset().top;
    var offset_left = $element.offset().left;

    var element_size = $element.width();
    var start_size = $circle.width();
    var finish_size = element_size * 4;

    var circle_top = event.clientY - offset_top - start_size/2;
    var circle_left = event.clientX - offset_left - start_size/2;


    $circle.css({'top': circle_top,'left': circle_left});
    $circle.animate({'opacity':'1', 'width': finish_size, 'height': finish_size, 'marginLeft':-finish_size/2,'marginTop':-finish_size/2}, 800)
  });

  $('body').on('mouseup', function(){
    if($circle != ''){
      $circle.remove();
      $circle = '';
    }
  });

  $('.page').scroll(function(event){
    event.stopPropagation();
    event.preventDefault();
    console.log($('.page').scrollTop());
    return false;
  });

  if (elem.addEventListener) {
    if ('onwheel' in document) {
      // IE9+, FF17+, Ch31+
      elem.addEventListener("wheel", onWheel);
    } else if ('onmousewheel' in document) {
      // устаревший вариант события
      elem.addEventListener("mousewheel", onWheel);
    } else {
      // Firefox < 17
      elem.addEventListener("MozMousePixelScroll", onWheel);
    }
  } else { // IE8-
    elem.attachEvent("onmousewheel", onWheel);
  }

  function onWheel(e) {
    e = e || window.event;

    // wheelDelta не дает возможность узнать количество пикселей
    var delta = e.deltaY || e.detail || e.wheelDelta;
    scrollY += delta;
    console.log(scrollY);

    $('#page').scrollTop(scrollY);
    e.preventDefault ? e.preventDefault() : (e.returnValue = false);
  }

});
$(window).on("load", function(){
  $('#site-overlay .cssload').css('display', 'none');
  $('#site-overlay').animate({'opacity': 0}, 1000);
  setTimeout(function () {
    $('#site-overlay').css('display', 'none');
  }, 1000);
});

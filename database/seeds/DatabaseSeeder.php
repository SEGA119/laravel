<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(PostsTableSeeder::class);
    }
}

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        if(App\User::where('email', 'shumakov119@gmail.com')->get()->isEmpty()){
          $this->god_creation();
        }

        $faker = Faker\Factory::create();
        $gender = array( '0' => 'female', '1' => 'male' );

        for( $i = 0; $i<100; $i++){
          App\User::create([
            'email' => $faker->unique()->email,
            'nickname' => $faker->unique()->word(),
            'gender' => $gender[rand(0,1)],
            'password'  => bcrypt('test_pass'),
            'first_name' => $faker->firstName(),
            'last_name' => $faker->lastName(),
            'avatar_url' => url('laravel/image999')
          ]);
        }
    }

    public function god_creation(){
      App\User::create([
        'email' => 'shumakov119@gmail.com',
        'nickname' => 'sega119',
        'gender' => 'male',
        'password'  => bcrypt('s22676722'),
        'first_name' => 'Sergey',
        'last_name' => 'Shumakov',
        'role' => 'admin',
        'country' => 'Ukraine',
        'avatar_url' => url('laravel/image1000000')
      ]);
    }
}

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $faker = Faker\Factory::create();

        for( $i = 0; $i<100; $i++){
          App\Post::create([
            'authorID' => App\User::orderBy(DB::raw('RAND()'))->first()->id,
            'description' => $faker->realText(rand(150,200)),
            'attached_image' => url('laravel/image999'),
            'created_at' => $faker->dateTime()
          ]);
        }
    }
}

class RelationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 64)->unique();
            $table->string('password', 128);
            $table->string('nickname', 64)->unique();
            $table->enum('gender', ['female','male'])->nullable()->default('male');
            $table->string('first_name', 64)->nullable()->default('Mr.');
            $table->string('last_name', 64)->nullable()->default('Noname');
            $table->enum('role', ['admin','editor','user'])->default('user');
            $table->string('country', 64)->nullable();
            $table->string('avatar_url')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}

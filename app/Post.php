<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = ['authorID','description','time','attached_image'];

    public function get_public_events(){
    }
}

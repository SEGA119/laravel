<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/* Application routes */
Route::group([], function(){

  /* Authorization routes */
  Route::group([], function(){
    Route::get('/login', ['as' => 'login', 'uses' => 'UserAuth@login_view']);
    Route::post('/login', ['uses' => 'UserAuth@login']);

    Route::get('/register', ['as' => 'register', 'uses' => 'UserAuth@register_view']);
    Route::post('/register', 'UserAuth@register');

    Route::get('/logout', ['as' => 'logout', 'middleware' => 'auth', 'uses' => 'UserAuth@logout']);
  });

  /* User routes */
  Route::group([], function(){
    Route::get('/user/{nickname}', ['as' => 'user', 'uses' => 'UserController@index']);

    Route::get('/profile', ['as'=>'profile', 'uses'=> 'UserController@profile']);

    Route::get('/song/upload', ['as' => 'song_upload', 'uses'=> 'SongController@upload_view']);
    Route::post('/song/upload', ['uses'=> 'SongController@upload']);
  });

  /* Admin routes */
  Route::group(['prefix' => '/admin', 'as'=>'admin'], function(){
    Route::get('/presentation', function(){
      return view('presentation');
    });
    Route::post('/presentation', function( Illuminate\Http\Request $request){
      if( $action = $request->input('action') && $name = $request->input('name') ){
        $response = array();
        if($action == 'start'){
          $response = [
            'status'=> 'success',
            'link'  => ''
          ];
          return Response::json($response, 200);
        }elseif($action == 'next'){
          return Response::json($response, 200);
        }elseif($action == 'prev'){
          return Response::json($response, 200);
        }
        return Response::json($response, 400);
      }
      else{
        return Response::json($response, 400);
      }
    });
  });

  /* Api Routes */
  Route::group(['prefix' => '/api', 'as' => 'api'], function(){
    Route::post('/user/add', function(){
      return Response::json(json_encode([
        'status' => 'success',
        'message' => $_POST
      ]), 200);
    });
    Route::get('/user/{nickname}', function($nickname){
      try{
        $user = App\User::where( 'nickname', '=', $nickname )->first()->get_json_info();
        return Response::json( $user, 200);
      }
      catch(Exception $e){
        return Response::json( '', 400);
      }
    });
  });

  /* Other Routes */
  Route::group([], function(){
    Route::get('/', [ 'as'=>'home', 'uses' => 'HomeController@index']);

    Route::get('/customers',function(){
      $gender = array( 'female','male' );
      echo  $gender[rand(0,1)];
    });
    Route::get('/image{id}', [ 'as' => 'image', function($id){
      header('Content-type: image/jpeg');
      echo file_get_contents(URL::to('/img').'/'.$id.".jpg");
    }]);
  });

});

Route::group([], function(){

  /* User Pages routes */



});

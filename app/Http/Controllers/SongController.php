<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use App\Http\Requests;
use Auth;

class SongController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index( $nickname ){
    $page_template = 'user';
    $content_template = 'profile';

    $page_data = array(
      'user_info' => User::where( 'nickname', '=', $nickname )->first()->get_protected_info(),
      // 'relations' => Relations::where('')
      'page'      => $page_template,
      'content'   => $content_template
    );

    return view('user', $page_data);
  }

  public function upload_view(){
    $page_template = 'song';
    $content_template = 'upload';

    $page_data = array(
      'user_info' => Auth::user()->get_protected_info(),
      'page'      => $page_template,
      'content'   => $content_template
    );

    return view('song_upload', $page_data);
  }

  public function upload( Request $request ){
    // if($request->file('file')->isValid()){
      // return dd($request->input());
    // }
    // else{
      // return 'heeeey...';
    // }
  }
}

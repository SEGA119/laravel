<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Auth;
use App\User;

class UserAuth extends Controller
{
    // protected $redirectTo = '/';

    public function __construct(){

    }

    public function index(){

    }

    protected function login( Request $request ){
      try{
        $user = User::where('email', $request->input('email'))->first();

        if(!$user){
          $response = json_encode([
            'status' => 'fail',
            'message' => 'User not exists'
          ]);
          return Response::json($response, 200);
        }
        else{
          if (Auth::attempt(array('email' => $request->input('email'), 'password' => $request->input('password') )))
          {
            $response = json_encode([
              'status' => 'success',
              'message' => 'Login succesfull!'
            ]);
            return Response::json($response, 200);
          }
          else{
            $response = json_encode([
              'status' => 'fail',
              'message' => 'Wrong credentials'
            ]);
            return Response::json($response, 200);
          }
        }
      }
      catch(Exception $e){
        $response = json_encode([
            'status' => 'error',
            'message' => 'Bad request'
        ]);
        return Response::json($request, 400);
      }
    }

    public function login_view( ){
      if(Auth::check()){
        return redirect('/');
      }
      else{
        $page_template = 'auth';
        $content_template = 'login';

        $page_data = array(
          'page'      => $page_template,
          'content'   => $content_template
        );

        return view('auth/login', $page_data);
      }
    }

    public function register_view( ){
      if(Auth::check()){
        echo 'lol;';
        return redirect('/');
      }
      else{
        $page_template = 'auth';
        $content_template = 'register';

        $page_data = array(
          'page'      => $page_template,
          'content'   => $content_template
        );

        return view('auth/register', $page_data);
      }
    }

    protected function register( Request $request ){
        $user_info = array(
          'nickname' => $request->input('nickname'),
          'email' => $request->input('email'),
          'password' => bcrypt($request->input('password'))
        );
        if( $id = User::create($user_info)->id ){
          Auth::loginUsingId($id, false);
          // Mail::send('emails.welcome', $data, function($message)
          // {
          //     $message->from('us@example.com', 'Laravel');
          //
          //     $message->to('foo@example.com')->cc('bar@example.com');
          //
          //     $message->attach($pathToFile);
          // });
          return redirect('/');
        }
    }

    protected function logout(){
      Auth::logout();
      return redirect('login');
    }
}

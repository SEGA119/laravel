<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\User;

class UserController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index( $nickname ){
    $page_template = 'user';
    $content_template = 'profile';

    $page_data = array(
      'user_info' => User::where( 'nickname', '=', $nickname )->first()->get_protected_info(),
      // 'relations' => Relations::where('')
      'page'      => $page_template,
      'content'   => $content_template
    );

    return view('user', $page_data);
  }

}

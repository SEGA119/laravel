<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $page_template = 'main';
      $content_template = 'posts';
      $posts = DB::table('users')
      ->join('posts', 'posts.authorID', '=', 'users.id')
      ->select('users.nickname',
        'users.first_name',
        'users.last_name',
        'users.avatar_url',
        'posts.attached_image',
        'posts.description',
        'posts.created_at'
        )
      ->take(10)
      ->get();

      $page_data = array(
        'user_info' => Auth::user()->get_protected_info(),
        'posts'    => $posts,
        'page'      => $page_template,
        'content'   => $content_template
      );

      return view('main', $page_data);
    }
}

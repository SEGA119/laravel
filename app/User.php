<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';
    protected $fillable = ['email','password','nickname','first_name','last_name','role','country','avatar_url'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function get_slug(){
      return mb_strtolower( $this->nickname );
    }

    public function get_protected_info(){
      return [
        'nickname'    => $this->nickname,
        'email'       => $this->email,
        'first_name'  => $this->first_name,
        'last_name'   => $this->last_name,
        'country'     => $this->country,
        'role'        => $this->role,
        'avatar_url'  => $this->avatar_url
      ];
    }

    public function get_json_info(){
      return json_encode([
        'nickname'    => $this->nickname,
        'email'       => $this->email,
        'first_name'  => $this->first_name,
        'last_name'   => $this->last_name,
        'country'     => $this->country,
        'role'        => $this->role,
        'avatar_url'  => $this->avatar_url
      ]);
    }
}

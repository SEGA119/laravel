@extends('layouts.app')

@section('page')

  @section('sidebar')

  @show
  <div id="site-content" class="content {{$content or 'default'}}">
    @yield('content')
  </div>

  @section('modal')

  @show

@endsection

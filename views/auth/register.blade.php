@extends('layouts.page')

@section('sidebar')

@stop

@section('content')
  <div class="auth-form">
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
        {{ csrf_field() }}

        <a id="site-title" class="white-bg" href="{{ route('home')  }}"><span>#</span>Music</a>
        <div class="form-group{{ $errors->has('nickname') ? ' has-error' : '' }}">
          <input id="nickname" type="text" class="form-control" name="nickname" placeholder="Nickname" value="{{ old('nickname') }}">

          @if ($errors->has('nickname'))
              <span class="help-block">
                  <strong>{{ $errors->first('nickname') }}</strong>
              </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          <input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}">

          @if ($errors->has('email'))
              <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <input id="password" type="password" class="form-control" placeholder="Password" name="password">

          @if ($errors->has('password'))
              <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
          <input id="password-confirm" type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation">

          @if ($errors->has('password_confirmation'))
              <span class="help-block">
                  <strong>{{ $errors->first('password_confirmation') }}</strong>
              </span>
          @endif
        </div>

        <div class="form-group">
          <button type="submit" class="btn">
              <i class="fa fa-btn fa-user"></i> Register
          </button>
        </div>
    </form>
    <a class="with-shadow" href="{{ route('login') }}">Already have account?</a>    
  </div>
@endsection

@extends('layouts.page')

@section('sidebar')

@stop

@section('content')
  <div class="auth-form">
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
      {{ csrf_field() }}

      <a id="site-title" class="white-bg" href="{{ route('home')  }}"><span>#</span>Music</a>
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}">

        @if ($errors->has('email'))
        <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
      </div>

      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <input id="password" type="password" class="form-control" name="password" placeholder="Password">

        @if ($errors->has('password'))
        <span class="help-block">
          <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
      </div>

      <div class="form-group">

        <div class="checkbox">
          <label>
            <input type="checkbox" name="remember"> Remember Me
          </label>
        </div>
      </div>

      <div class="form-group">
        <button type="submit" class="btn">
          <i class="fa fa-btn fa-sign-in"></i> Login
        </button>

      </div>
      @if( Session::has('flash_message'))
      <h2>{{ Session::get('flash_message') }}</h2>
      @endif
    </form>
    <a class="with-shadow" href="{{ route('register') }}">Want to join us?</a>
  </div>
@endsection

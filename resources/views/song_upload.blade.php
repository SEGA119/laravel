@extends('layouts.page')

@section('sidebar')

<nav class="nav-sidebar">
  <a id="site-title" href="{{ route('home')  }}"><span>#</span>Music</a>
  <ul class="nav-links">
    <li class="{{ (Request::is('song/upload')) ? 'active' : '' }}"><a class="nav-link" href="{{ route('song_upload') }}">Add Song</a></li>
    <li class="{{ (Request::is('profile')) ? 'active' : '' }}"><a class="nav-link" href="#">Profile</a></li>
    <li><a class="nav-link" href="#">Menu</a></li>
    <li><a class="nav-link" href="{{ route('logout') }}">Log Out</a></li>
  </ul>
  <div class="profile_menu">
    <div class="user_wrap">
      <div class="user_name">{{ $user_info['nickname'] }}</div>
      <img src="{{ $user_info['avatar_url'] or 'default' }}" alt="" />
    </div>
    <div class="profile_menu_item"><i class="fa fa-bell-o"></i></div>
    <div class="profile_menu_item"><i class="fa fa-envelope-o"></i></div>
  </div>
</nav>

@endsection

@section('content')

<div class="form_wrap">
  <div style="width:100px; height: 100px; background-color: #fff;" class="modal-toggle" data-target="dropzone-modal"></div>
  <form id='ajax' action="{{ route('song_upload') }}" method="POST">
    <input type='file' name='file'  />
    {{ $user_info['avatar_url'] }}
    <input type='submit' value="test it!"  />
  </form>
</div>

@endsection

@section('modal')

<div id="dropzone-modal" class="modal">
  <div class="modal_wrap">
    <div class="modal_window">
      <form id="song-upload-dropzone" class="dropzone" action="{{ route('song_upload') }}" method="POST">
        {{ csrf_field() }}
        <div class='loading-overlay'>
          <div class='cssload'>
            <div class='cssload-thecube'>
              <div class='cssload-cube cssload-c1'></div>
              <div class='cssload-cube cssload-c2'></div>
              <div class='cssload-cube cssload-c4'></div>
              <div class='cssload-cube cssload-c3'></div>
            </div>
          </div>
        </div>
      </form>
      <div class="modal_controls">
        <span class="modal_exit modal_control"><i class="fa fa-times"></i></span>
        <span style="display: none;" class="modal_control"><i class="fa fa-square-o"></i></span>
        <span style="display: none;" class="modal_control"><i class="fa fa-minus"></i></span>
      </div>
    </div>
  </div>
</div>

@endsection

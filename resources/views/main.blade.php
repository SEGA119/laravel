@extends('layouts.page')

@section('sidebar')

<nav class="nav-sidebar">
  <a id="site-title" href="{{ route('home')  }}"><span>#</span>Music</a>
  <ul class="nav-links">
    <li class="{{ (Request::is(route('song_upload'))) ? 'active' : '' }}"><a class="nav-link" href="{{ route('song_upload') }}">Add Song</a></li>
    <li class="{{ (Request::is(route('profile'))) ? 'active' : '' }}"><a class="nav-link" href="#">Profile</a></li>
    <li><a class="nav-link" href="#">Menu</a></li>
    <li><a class="nav-link" href="logout">Log Out</a></li>
  </ul>
  <div class="profile_menu">
    <div class="user_wrap">
      <div class="user_name">{{ $user_info['nickname'] }}</div>
      <img src="{{ $user_info['avatar_url'] or 'default' }}" alt="" />
    </div>
    <div class="profile_menu_item"><i class="fa fa-bell-o"></i></div>
    <div class="profile_menu_item"><i class="fa fa-envelope-o"></i></div>
  </div>
</nav>

@endsection

@section('content')

  @foreach($posts as $post)
    <div class="post_wrap normal">
      <!-- <span ng-click='close()' ng-show="post_show" class="hide_post">X</span> -->
      <div class="header_info">
        <div class="photo_wrap">
          <a href="{{ route('user', $post->nickname) }}">
            <img class="post_avatar" src="{{ $post->avatar_url }}" alt="user"  />
          </a>
        </div>
        <div class="post_info">
          <h1><a href="{{ route('user', $post->nickname) }}">{{ $post->first_name.' '.$post->last_name }}</a></h1>
          <h2><a href="{{ route('user', $post->nickname) }}">@datetime($post->created_at)</h2></a>
        </div>
      </div>
      <div class="post_content">
        <img class="post_image" src="{{ $post->attached_image }}" alt="Post"  />
        <p>
          {{ $post->description }}
        </p>
      </div>
      <div class="post_links">
        <a href="#"><i class="fa fa-heart"></i></a>
      </div>
    </div>

  @endforeach

@endsection

@extends('layouts.app')

@section('page')
  <style>
    h1.html-code{
      display: inline-block;
      font-size: 4rem;
      color: #D6254D;
      text-transform: uppercase;
    }
  </style>
  <div class="centered_wrap">
    <div class="centered_block">
      <h1 class="html-code">Page Not Found 404</h1>
    </div>
  </div>
@endsection

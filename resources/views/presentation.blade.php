@extends('layouts/app')

@section('sidebar')

@endsection

@section('page')
<link href="{{ asset('css/presentation.css') }}" rel="stylesheet">
  <div id="presentation" ng-controller="presentation" class="presentation_wrap">
    <span ng-click="prev()" ng-show="controls.prev" class="presentation_controls fa fa-angle-left"></span>
    <!-- <img src="{{ route('image','1') }}" alt=""  /> -->
    <span ng-click="next()" ng-show="controls.next" class="presentation_controls fa fa-angle-right"></span>
    <i id="fullscreen" ng-click="fullscreen()" class="fa fa-arrows-alt fullscreen-btn" aria-hidden="true"></i>
  </div>
<script src="{{ asset('js/presentation.js') }}"></script>
@endsection

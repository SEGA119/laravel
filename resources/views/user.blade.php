@extends('layouts.page')

@section('content')

  <header id="profile-header">
    <div class="avatar_wrap">
      <img src="{{ $user_info['avatar_url'] }}" alt="" />
    </div>
  </header>
  <section id="profile-content">

  </section>


@endsection

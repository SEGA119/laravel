<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Oldenburg' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/reset.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <script src="{{ asset('js/angular.min.js') }}"></script>
    <script src="{{ asset('js/fullscreen.js') }}"></script>
    <script>
      var base_url = "{{ route('home') }}";
    </script>
</head>
<body class="app" ng-app="application" ng-controller="page" id="app-layout">
    <div id="page" class="page {{$page or 'default'}}" ng-class="{true: 'ready', false: 'not-ready'}[ready]">
      <div ng-show="!ready" id="site-overlay" class='loading-overlay'>
    		<div class='cssload'>
    			<div class='cssload-thecube'>
            <i class="fa fa-clock-o"></i>
    				<div class='cssload-cube cssload-c1'></div>
    				<div class='cssload-cube cssload-c2'></div>
    				<div class='cssload-cube cssload-c4'></div>
    				<div class='cssload-cube cssload-c3'></div>
    			</div>
    		</div>
    	</div>
      @yield('page')
    </div>
    <!-- JavaScripts -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    <script src="{{ asset('js/jquery-3.0.0.min.js') }}"></script>
    <script src="{{ asset('js/dropzone.js') }}"></script>
    <script src="{{ asset('js/id3.min.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    <!-- <script src="{{ asset('js/modal.js') }}"></script> -->
</body>
</html>

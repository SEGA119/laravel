@extends('layouts.page')

@section('sidebar')

@stop

@section('content')
  <div class="auth-form">
    <form ng-controller="ajax-form" ng-submit="save( '{{ url('/login') }}' )" class="form-horizontal" role="form">
      {{ csrf_field() }}
      <a id="site-title" class="white-bg" href="{{ route('home')  }}"><span>#</span>Music</a>
      <div class="form-group">
        <input id="email" ng-model="form.email" type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}">
      </div>

      <div class="form-group">
        <input id="password" ng-model="form.password" type="password" class="form-control" name="password" placeholder="Password">
      </div>

      <div class="form-group">

        <div class="checkbox">
          <label>
            <input type="checkbox" ng-model="form.remember" name="remember"> Remember Me
          </label>
        </div>
      </div>

      <div class="form-group">
        <button type="submit" class="btn">
          <i class="fa fa-btn fa-sign-in"></i> Login
        </button>

      </div>
      <span class="form-message">@{{ response.message }}</span>
      @if( Session::has('flash_message'))
      <h2>@{{ Session::get('flash_message') }}</h2>
      @endif
    </form>
    <a class="with-shadow" href="{{ route('register') }}">Want to join us?</a>
  </div>
@endsection
